// Include the module generated from the .proto file
include!(concat!(env!("OUT_DIR"), "/transit_realtime.rs"));

use prost::Message;

use std::error::Error;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // Denver RTD realtime vehicle positions
    let url = "https://rtd.prod.acquia-sites.com/files/gtfs-rt/VehiclePosition.pb";

    let response = reqwest::get(url).await?;
    let bytes = response.bytes().await?;

  

    let feed = FeedMessage::decode(bytes.as_ref())?;

    dbg!(&feed);
    for entity in feed.entity {
        let vehicle = entity.vehicle.unwrap();
        let position = vehicle.position.unwrap();
        println!(
            "Vehicle ID: {}, Latitude: {}, Longitude: {}",
            vehicle.vehicle.unwrap().id.unwrap(),
            position.latitude,
            position.longitude
        );
    }
    Ok(())
}

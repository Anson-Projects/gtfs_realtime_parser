fn main() {
    prost_build::Config::new()
        .out_dir(std::env::var("OUT_DIR").unwrap())
        .compile_protos(&["proto/transit_realtime.proto"], &["proto/"]).unwrap();
}